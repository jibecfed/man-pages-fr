PERKVERSION = 3.75
L10N_PATCH = -1
PERK_L10N_VERSION = $(PERKVERSION)$(L10N_PATCH)

LANGS ?= fr
WORK_DIR = $(shell pwd)

export LANGS
export WORK_DIR

unpack: stamp-unpack

ifeq ($(findstring git,$(PERKVERSION)),)
stamp-unpack: stamp-unpack-release
	touch $@
else
stamp-unpack: stamp-unpack-git
	touch $@
endif

stamp-unpack-release: perkamon-$(PERKVERSION).tar.xz
	-rm -rf perkamon perkamon-$(PERKVERSION)
	tar Jxf perkamon-$(PERKVERSION).tar.xz
	mv perkamon-$(PERKVERSION) perkamon
	#  Sometimes we have to update perkamon/Makefile
	test ! -f Makefile.man-pages || cp Makefile.man-pages perkamon/Makefile
	#  Remove stamp-setup to force re-run of 'setup' target
	-rm -f stamp-setup
	touch $@

stamp-unpack-git:
	$(MAKE) clean
	-@git submodule add git://gitorious.org/perkamon/man-pages.git perkamon-git
	git submodule init && git submodule update --recursive
	ln -s perkamon-git perkamon
	$(MAKE) -C perkamon clean unpack
	#  Remove stamp-setup to force re-run of 'setup' target
	-rm -f stamp-setup
	touch $@

setup: stamp-setup
stamp-setup: stamp-unpack
	$(MAKE) -C perkamon setup
	mv perkamon/build .
	touch $@

#  Download tarball
get-orig-source: perkamon-$(PERKVERSION).tar.xz
perkamon-$(PERKVERSION).tar.xz:
	p=$(PERKVERSION); wget http://perkamon.alioth.debian.org/archives/$${p%-*}/$@

update-colophon:
	find po4a -name $(LANGS).po -o -name \*.pot | xargs sed -i -e 's,This page is part of release [^ ]*,This page is part of release $(PERKVERSION),' -e 's,Cette page fait partie de la publication [^ ]*,Cette page fait partie de la publication $(PERKVERSION),'

translate: setup
translate stats disable-removed print-new-files:
	test -e perkamon || $(MAKE) unpack
	$(MAKE) -C perkamon $@

cfg-%: FORCE
	test -e perkamon || $(MAKE) unpack
	$(MAKE) -C perkamon $@

clean::
	-rm -f stamp-*
	-rm -rf perkamon perkamon-$(PERKVERSION) perkamon-$(LANGS)*
	-rm -rf build
	-rm -f po4a/*/po/*.po~
	-rm -f tmp-date

dist-%: translate
	cp README.$* build/$*/
	tar Jcf man-pages-$*-$(PERK_L10N_VERSION).tar.xz  --numeric-owner -C build $*

release: clean perkamon-$(PERKVERSION).tar.xz
	-rm -rf perkamon-$(LANGS)*
	mkdir perkamon-$(LANGS)-$(PERK_L10N_VERSION)
	cp perkamon-$(PERKVERSION).tar.xz perkamon-$(LANGS)-$(PERK_L10N_VERSION)/
	cp Makefile* README* perkamon-$(LANGS)-$(PERK_L10N_VERSION)/
	tar cf - --exclude=.svn po4a | tar xf - -C perkamon-$(LANGS)-$(PERK_L10N_VERSION)
	tar Jchf perkamon-$(LANGS)-$(PERK_L10N_VERSION).tar.xz  --numeric-owner perkamon-$(LANGS)-$(PERK_L10N_VERSION)

# Automatically update dates translations
translate-dates:
	@for f in po4a/*/po/$(LANGS).po; do \
	./translate-dates.pl < $$f > tmp-date && mv tmp-date $$f ; \
	done

format:
	for f in po4a/*/po/$(LANGS).po; do msgcat -o $$f $$f; done

clear-previous:
	for f in po4a/*/po/$(LANGS).po; do msgattrib --clear-previous -o $$f $$f ; done

.PHONY: unpack setup translate stats disable-removed print-new-files clean release FORCE format
