# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2014-07-13 00:45+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: build/C/man3/iconv.3:1
#, no-wrap
msgid "ICONV"
msgstr ""

#. type: TH
#: build/C/man3/iconv.3:2
#, no-wrap
msgid "2014-06-13"
msgstr ""

#. type: TH
#: build/C/man3/iconv.3:3 build/C/man3/iconv_close.3:3 build/C/man3/iconv_open.3:3
#, no-wrap
msgid "GNU"
msgstr ""

#. type: TH
#: build/C/man3/iconv.3:4 build/C/man3/iconv_close.3:4 build/C/man3/iconv_open.3:4
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#. type: SH
#: build/C/man3/iconv.3:5 build/C/man3/iconv_close.3:5 build/C/man3/iconv_open.3:5
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:6
msgid "iconv - perform character set conversion"
msgstr ""

#. type: SH
#: build/C/man3/iconv.3:7 build/C/man3/iconv_close.3:7 build/C/man3/iconv_open.3:7
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:8 build/C/man3/iconv_close.3:8 build/C/man3/iconv_open.3:8
#, no-wrap
msgid "B<#include E<lt>iconv.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:9
#, no-wrap
msgid ""
"B<size_t iconv(iconv_t >I<cd>B<,>\n"
"B<             char **>I<inbuf>B<, size_t *>I<inbytesleft>B<,>\n"
"B<             char **>I<outbuf>B<, size_t *>I<outbytesleft>B<);>\n"
msgstr ""

#. type: SH
#: build/C/man3/iconv.3:10 build/C/man3/iconv_close.3:10 build/C/man3/iconv_open.3:10
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:11
msgid ""
"The B<iconv>()  function converts a sequence of characters in one character "
"encoding to a sequence of characters in another character encoding.  The "
"I<cd> argument is a conversion descriptor, previously created by a call to "
"B<iconv_open>(3); the conversion descriptor defines the character encodings "
"that B<iconv>()  uses for the conversion.  The I<inbuf> argument is the "
"address of a variable that points to the first character of the input "
"sequence; I<inbytesleft> indicates the number of bytes in that buffer.  The "
"I<outbuf> argument is the address of a variable that points to the first "
"byte available in the output buffer; I<outbytesleft> indicates the number of "
"bytes available in the output buffer."
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:12
msgid ""
"The main case is when I<inbuf> is not NULL and I<*inbuf> is not NULL.  In "
"this case, the B<iconv>()  function converts the multibyte sequence starting "
"at I<*inbuf> to a multibyte sequence starting at I<*outbuf>.  At most "
"I<*inbytesleft> bytes, starting at I<*inbuf>, will be read.  At most "
"I<*outbytesleft> bytes, starting at I<*outbuf>, will be written."
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:13
msgid ""
"The B<iconv>()  function converts one multibyte character at a time, and for "
"each character conversion it increments I<*inbuf> and decrements "
"I<*inbytesleft> by the number of converted input bytes, it increments "
"I<*outbuf> and decrements I<*outbytesleft> by the number of converted output "
"bytes, and it updates the conversion state contained in I<cd>.  If the "
"character encoding of the input is stateful, the B<iconv>()  function can "
"also convert a sequence of input bytes to an update to the conversion state "
"without producing any output bytes; such input is called a I<shift "
"sequence>.  The conversion can stop for four reasons:"
msgstr ""

#. type: IP
#: build/C/man3/iconv.3:14
#, no-wrap
msgid "1."
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:15
msgid ""
"An invalid multibyte sequence is encountered in the input.  In this case, it "
"sets I<errno> to B<EILSEQ> and returns I<(size_t)\\ -1>.  I<*inbuf> is left "
"pointing to the beginning of the invalid multibyte sequence."
msgstr ""

#. type: IP
#: build/C/man3/iconv.3:16
#, no-wrap
msgid "2."
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:17
msgid ""
"The input byte sequence has been entirely converted, that is, "
"I<*inbytesleft> has gone down to 0.  In this case, B<iconv>()  returns the "
"number of nonreversible conversions performed during this call."
msgstr ""

#. type: IP
#: build/C/man3/iconv.3:18
#, no-wrap
msgid "3."
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:19
msgid ""
"An incomplete multibyte sequence is encountered in the input, and the input "
"byte sequence terminates after it.  In this case, it sets I<errno> to "
"B<EINVAL> and returns I<(size_t)\\ -1>.  I<*inbuf> is left pointing to the "
"beginning of the incomplete multibyte sequence."
msgstr ""

#. type: IP
#: build/C/man3/iconv.3:20
#, no-wrap
msgid "4."
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:21
msgid ""
"The output buffer has no more room for the next converted character.  In "
"this case, it sets I<errno> to B<E2BIG> and returns I<(size_t)\\ -1>."
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:22
msgid ""
"A different case is when I<inbuf> is NULL or I<*inbuf> is NULL, but "
"I<outbuf> is not NULL and I<*outbuf> is not NULL.  In this case, the "
"B<iconv>()  function attempts to set I<cd>'s conversion state to the initial "
"state and store a corresponding shift sequence at I<*outbuf>.  At most "
"I<*outbytesleft> bytes, starting at I<*outbuf>, will be written.  If the "
"output buffer has no more room for this reset sequence, it sets I<errno> to "
"B<E2BIG> and returns I<(size_t)\\ -1>.  Otherwise, it increments I<*outbuf> "
"and decrements I<*outbytesleft> by the number of bytes written."
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:23
msgid ""
"A third case is when I<inbuf> is NULL or I<*inbuf> is NULL, and I<outbuf> is "
"NULL or I<*outbuf> is NULL.  In this case, the B<iconv>()  function sets "
"I<cd>'s conversion state to the initial state."
msgstr ""

#. type: SH
#: build/C/man3/iconv.3:24 build/C/man3/iconv_close.3:12 build/C/man3/iconv_open.3:19
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:25
msgid ""
"The B<iconv>()  function returns the number of characters converted in a "
"nonreversible way during this call; reversible conversions are not counted.  "
"In case of error, it sets I<errno> and returns I<(size_t)\\ -1>."
msgstr ""

#. type: SH
#: build/C/man3/iconv.3:26 build/C/man3/iconv_open.3:21
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:27
msgid "The following errors can occur, among others:"
msgstr ""

#. type: TP
#: build/C/man3/iconv.3:28
#, no-wrap
msgid "B<E2BIG>"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:29
msgid "There is not sufficient room at I<*outbuf>."
msgstr ""

#. type: TP
#: build/C/man3/iconv.3:30
#, no-wrap
msgid "B<EILSEQ>"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:31
msgid "An invalid multibyte sequence has been encountered in the input."
msgstr ""

#. type: TP
#: build/C/man3/iconv.3:32 build/C/man3/iconv_open.3:23
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:33
msgid "An incomplete multibyte sequence has been encountered in the input."
msgstr ""

#. type: SH
#: build/C/man3/iconv.3:34 build/C/man3/iconv_close.3:14 build/C/man3/iconv_open.3:25
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:35 build/C/man3/iconv_close.3:15 build/C/man3/iconv_open.3:26
msgid "This function is available in glibc since version 2.1."
msgstr ""

#. type: SH
#: build/C/man3/iconv.3:36
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: SS
#: build/C/man3/iconv.3:37
#, no-wrap
msgid "Multithreading (see pthreads(7))"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:38
msgid "The B<iconv>()  function is thread-safe."
msgstr ""

#. type: SH
#: build/C/man3/iconv.3:39 build/C/man3/iconv_close.3:16 build/C/man3/iconv_open.3:27
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:40
msgid "POSIX.1-2001."
msgstr ""

#. type: SH
#: build/C/man3/iconv.3:41
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:42
msgid ""
"Although I<inbuf> and I<outbuf> are typed as I<char\\ **>, this does not "
"mean that the objects they point can be interpreted as C strings or as "
"arrays of characters: the interpretation of character byte sequences is "
"handled internally by the conversion functions.  In some encodings, a zero "
"byte may be a valid part of a multibyte character."
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:43
msgid ""
"The caller of B<iconv>()  must ensure that the pointers passed to the "
"function are suitable for accessing characters in the appropriate character "
"set.  This includes ensuring correct alignment on platforms that have tight "
"restrictions on alignment."
msgstr ""

#. type: SH
#: build/C/man3/iconv.3:44 build/C/man3/iconv_close.3:18 build/C/man3/iconv_open.3:29
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:45
msgid "B<iconv_close>(3), B<iconv_open>(3), B<iconvconfig>(8)"
msgstr ""

#. type: SH
#: build/C/man3/iconv.3:46 build/C/man3/iconv_close.3:20 build/C/man3/iconv_open.3:31
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv.3:47 build/C/man3/iconv_close.3:21 build/C/man3/iconv_open.3:32
msgid ""
"This page is part of release 3.75 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at "
"\\%http://www.kernel.org/doc/man-pages/."
msgstr ""

#. type: TH
#: build/C/man3/iconv_close.3:1
#, no-wrap
msgid "ICONV_CLOSE"
msgstr ""

#. type: TH
#: build/C/man3/iconv_close.3:2 build/C/man3/iconv_open.3:2
#, no-wrap
msgid "2008-08-11"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv_close.3:6
msgid "iconv_close - deallocate descriptor for character set conversion"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv_close.3:9
#, no-wrap
msgid "B<int iconv_close(iconv_t >I<cd>B<);>\n"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv_close.3:11
msgid ""
"The B<iconv_close>()  function deallocates a conversion descriptor I<cd> "
"previously allocated using B<iconv_open>(3)."
msgstr ""

#. type: Plain text
#: build/C/man3/iconv_close.3:13
msgid ""
"When successful, the B<iconv_close>()  function returns 0.  In case of "
"error, it sets I<errno> and returns -1."
msgstr ""

#. type: Plain text
#: build/C/man3/iconv_close.3:17 build/C/man3/iconv_open.3:28
msgid "UNIX98, POSIX.1-2001."
msgstr ""

#. type: Plain text
#: build/C/man3/iconv_close.3:19
msgid "B<iconv>(3), B<iconv_open>(3)"
msgstr ""

#. type: TH
#: build/C/man3/iconv_open.3:1
#, no-wrap
msgid "ICONV_OPEN"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv_open.3:6
msgid "iconv_open - allocate descriptor for character set conversion"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv_open.3:9
#, no-wrap
msgid ""
"B<iconv_t iconv_open(const char *>I<tocode>B<, const char "
"*>I<fromcode>B<);>\n"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv_open.3:11
msgid ""
"The B<iconv_open>()  function allocates a conversion descriptor suitable for "
"converting byte sequences from character encoding I<fromcode> to character "
"encoding I<tocode>."
msgstr ""

#. type: Plain text
#: build/C/man3/iconv_open.3:12
msgid ""
"The values permitted for I<fromcode> and I<tocode> and the supported "
"combinations are system-dependent.  For the GNU C library, the permitted "
"values are listed by the I<iconv --list> command, and all combinations of "
"the listed values are supported.  Furthermore the GNU C library and the GNU "
"libiconv library support the following two suffixes:"
msgstr ""

#. type: TP
#: build/C/man3/iconv_open.3:13
#, no-wrap
msgid "//TRANSLIT"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv_open.3:14
msgid ""
"When the string \"//TRANSLIT\" is appended to I<tocode>, transliteration is "
"activated.  This means that when a character cannot be represented in the "
"target character set, it can be approximated through one or several "
"similarly looking characters."
msgstr ""

#. type: TP
#: build/C/man3/iconv_open.3:15
#, no-wrap
msgid "//IGNORE"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv_open.3:16
msgid ""
"When the string \"//IGNORE\" is appended to I<tocode>, characters that "
"cannot be represented in the target character set will be silently "
"discarded."
msgstr ""

#. type: Plain text
#: build/C/man3/iconv_open.3:17
msgid ""
"The resulting conversion descriptor can be used with B<iconv>(3)  any number "
"of times.  It remains valid until deallocated using B<iconv_close>(3)."
msgstr ""

#. type: Plain text
#: build/C/man3/iconv_open.3:18
msgid ""
"A conversion descriptor contains a conversion state.  After creation using "
"B<iconv_open>(), the state is in the initial state.  Using B<iconv>(3)  "
"modifies the descriptor's conversion state.  (This implies that a conversion "
"descriptor can not be used in multiple threads simultaneously.)  To bring "
"the state back to the initial state, use B<iconv>(3)  with NULL as I<inbuf> "
"argument."
msgstr ""

#. type: Plain text
#: build/C/man3/iconv_open.3:20
msgid ""
"The B<iconv_open>()  function returns a freshly allocated conversion "
"descriptor.  In case of error, it sets I<errno> and returns I<(iconv_t)\\ "
"-1>."
msgstr ""

#. type: Plain text
#: build/C/man3/iconv_open.3:22
msgid "The following error can occur, among others:"
msgstr ""

#. type: Plain text
#: build/C/man3/iconv_open.3:24
msgid ""
"The conversion from I<fromcode> to I<tocode> is not supported by the "
"implementation."
msgstr ""

#. type: Plain text
#: build/C/man3/iconv_open.3:30
msgid "B<iconv>(1), B<iconv>(3), B<iconv_close>(3)"
msgstr ""
